module.exports = {
	tabWidth: 1,
	useTabs: true,
	singleQuote: true,
	overrides: [
		{
			files: ['*.yml', '*.yaml'],
			options: {
				tabWidth: 2,
				singleQuote: false,
			},
		},
	],
};
