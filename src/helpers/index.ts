import { IScrumBoard } from '../types';

/** Вычисляет из полученных данных максимальный id карточки */
export const getLastCardId = ({ cards }: IScrumBoard): number =>
	Math.max(
		...cards.reduce((acc, card) => [...acc, card], []).map((card) => card.id)
	);

/** Вычисляет время по UTC */
export const getNowDateUTC = () => {
	const now = new Date();
	return Date.UTC(
		now.getUTCFullYear(),
		now.getUTCMonth(),
		now.getUTCDate(),
		now.getUTCHours(),
		now.getUTCMinutes(),
		now.getUTCSeconds(),
		now.getUTCMilliseconds()
	);
};
