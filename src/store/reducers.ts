import { TSCrumBoardActionTypes } from './types';
import { TOGGLE_MODAL, ADD_CARD, HIDE_CARD, UPDATE_CARD } from './actions';

import { getLastCardId } from '../helpers';
import { IScrumBoard, IModalData } from '../types';
import { data } from '../../data.json';

export interface IScrumBoardState extends IScrumBoard {
	/** Идентификатор последней созданной карточки */
	idLastCreatedCard: number;
	/** Данные для модального окна */
	modal: IModalData;
}

const dataTyped = data as IScrumBoard;

const initialState: IScrumBoardState = {
	idLastCreatedCard: getLastCardId(dataTyped),
	columns: dataTyped.columns,
	cards: dataTyped.cards,
	modal: {
		isOpen: false,
		cardId: undefined,
	},
};

export const scrumBoardReducer = (
	state = initialState,
	action: TSCrumBoardActionTypes
): IScrumBoardState => {
	const { cards } = state;

	switch (action.type) {
		case ADD_CARD:
			return {
				...state,
				idLastCreatedCard: ++state.idLastCreatedCard,
				cards: [...cards, action.payload],
			};
		case UPDATE_CARD:
			return {
				...state,
				cards: cards.map((card) => {
					if (card.id === action.payload.id) {
						return action.payload;
					}

					return card;
				}),
			};
		case HIDE_CARD: {
			return {
				...state,
				cards: cards.map((card) => {
					if (card.id === action.payload) {
						return { ...card, isHidden: true };
					}

					return card;
				}),
			};
		}
		case TOGGLE_MODAL:
			return { ...state, modal: action.payload };
		default:
			return state;
	}
};
