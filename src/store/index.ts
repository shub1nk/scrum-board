import { combineReducers } from 'redux';

import { scrumBoardReducer } from './reducers';

export const rootReducer = combineReducers({
	board: scrumBoardReducer,
});

export type TRootState = ReturnType<typeof rootReducer>;
