import { TSCrumBoardActionTypes } from './types';

import { ICard, IModalData } from '../types';

export const ADD_CARD = 'ADD_CARD';
export const HIDE_CARD = 'HIDE_CARD';
export const UPDATE_CARD = 'UPDATE_CARD';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';

export const addCard = (card: ICard): TSCrumBoardActionTypes => ({
	type: ADD_CARD,
	payload: card,
});

export const updateCard = (card: ICard): TSCrumBoardActionTypes => ({
	type: UPDATE_CARD,
	payload: card,
});

export const hideCard = (id: number): TSCrumBoardActionTypes => ({
	type: HIDE_CARD,
	payload: id,
});

export const toggleModal = (state: IModalData): TSCrumBoardActionTypes => ({
	type: TOGGLE_MODAL,
	payload: state,
});
