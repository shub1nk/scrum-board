import { ADD_CARD, HIDE_CARD, UPDATE_CARD, TOGGLE_MODAL } from './actions';

import type { ICard, IModalData } from '../types';

export interface IAddCardAction {
	type: typeof ADD_CARD;
	payload: ICard;
}

export interface IHideCardAction {
	type: typeof HIDE_CARD;
	payload: number;
}

export interface IUpdateCardAction {
	type: typeof UPDATE_CARD;
	payload: ICard;
}

export interface IToggleModalAction {
	type: typeof TOGGLE_MODAL;
	payload: IModalData;
}

export type TSCrumBoardActionTypes =
	| IAddCardAction
	| IUpdateCardAction
	| IToggleModalAction
	| IHideCardAction;
