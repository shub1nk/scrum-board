import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import {
	Paper,
	Grid,
	withStyles,
	createStyles,
	WithStyles,
} from '@material-ui/core';

import { Card } from './card';

import { ICard, IColumn } from '../types';
import { TRootState } from '../store/index';

const styles = createStyles({
	wrapper: { minHeight: 500, width: 250, paddingTop: 12, overflow: 'auto' },
	heading: { textAlign: 'center', margin: '0 0 12px' },
});

export type TColumn = IColumn & WithStyles<typeof styles>;

export const ColumnComponent: FC<TColumn> = ({ heading, code, classes }) => {
	const cards = useSelector<TRootState, ICard[]>(({ board }) =>
		board.cards
			.filter((card) => card.status === code && !card?.isHidden)
			.sort((a, b) => {
				if (a.createDate < b.createDate) {
					return -1;
				}
				if (a.createDate > b.createDate) {
					return 1;
				}
				return 0;
			})
	);

	return (
		<Grid key={heading} item>
			<Paper className={classes.wrapper}>
				<h3 className={classes.heading}>{heading}</h3>
				{cards.map((card) => (
					<Card key={card.id} {...card} />
				))}
			</Paper>
		</Grid>
	);
};

export const Column = withStyles(styles)(ColumnComponent);
