import React, { FC } from 'react';
import {
	Select as SelectMUI,
	MenuItem,
	createStyles,
	withStyles,
	WithStyles,
} from '@material-ui/core';

import type { ISelect } from '../types';

const styles = createStyles({
	selectField: { marginBottom: 12, display: 'block' },
});

export type TSelect = ISelect & WithStyles<typeof styles>;

const SelectComponent: FC<TSelect> = ({
	options,
	classes,
	value,
	onChangeHandler,
}) => {
	return (
		<div className={classes.selectField}>
			<SelectMUI value={value} onChange={onChangeHandler}>
				{options.map((option) => (
					<MenuItem key={option} value={option}>
						{option}
					</MenuItem>
				))}
			</SelectMUI>
		</div>
	);
};

export const SelectField = withStyles(styles)(SelectComponent);
