import React, {
	FC,
	FormEvent,
	useCallback,
	useState,
	ChangeEvent,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	Button,
	createStyles,
	withStyles,
	WithStyles,
} from '@material-ui/core';

import { TextField } from './text-field';
import { SelectField } from './select';

import type { TRootState } from '../store';
import type { TPriority, IForm, TColumnName } from '../types';
import { getNowDateUTC } from '../helpers';
import { toggleModal, addCard, updateCard } from '../store/actions';

const styles = createStyles({
	form: { padding: 24, textAlign: 'center' },
	textField: { marginBottom: 12, display: 'block' },
});

export type TForm = IForm & WithStyles<typeof styles>;

const FormComponent: FC<TForm> = ({ card, classes }) => {
	const [name, setName] = useState(card?.name || '');
	const [description, setDescription] = useState(card?.description || '');
	const [priority, setPriority] = useState<TPriority>(
		card?.priority || 'normal'
	);
	const [status, setStatus] = useState<TColumnName>(card?.status || 'todo');

	const lastCardId = useSelector<TRootState, number>(
		({ board }) => board.idLastCreatedCard
	);

	const dispatch = useDispatch();
	const closeModal = useCallback(
		() => dispatch(toggleModal({ isOpen: false })),
		[dispatch]
	);
	const addCardAction = useCallback((card) => dispatch(addCard(card)), [
		dispatch,
	]);
	const updateCardAction = useCallback((card) => dispatch(updateCard(card)), [
		dispatch,
	]);

	const handleOnSubmit = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		if (card) {
			updateCardAction({ ...card, name, description, priority, status });
		} else {
			addCardAction({
				id: lastCardId + 1,
				name,
				description,
				priority,
				createDate: getNowDateUTC(),
				status,
			});
		}
		closeModal();
	};

	const handleChangeName = (e: ChangeEvent<HTMLInputElement>) => {
		setName(e.target.value);
	};
	const handleChangeDesctiption = (e: ChangeEvent<HTMLInputElement>) => {
		setDescription(e.target.value);
	};
	const handleChangePriority = (event: ChangeEvent<{ value: TPriority }>) => {
		setPriority(event.target.value);
	};
	const handleChangeStatus = (event: ChangeEvent<{ value: TColumnName }>) => {
		setStatus(event.target.value);
	};

	return (
		<form className={classes.form} onSubmit={handleOnSubmit}>
			<h2>{card ? `Edit card - ${card.id}` : `Create new card`}</h2>
			<TextField
				id="name"
				label="name"
				value={name}
				onChangeHandler={handleChangeName}
			/>
			<TextField
				id="description"
				label="description"
				value={description}
				onChangeHandler={handleChangeDesctiption}
			/>
			<SelectField
				value={priority}
				onChangeHandler={handleChangePriority}
				options={['high', 'normal', 'low']}
			/>
			{card && (
				<SelectField
					value={status}
					onChangeHandler={handleChangeStatus}
					options={['todo', 'inProgress', 'done']}
				/>
			)}
			<Button type="submit" variant="contained" color="primary">
				{card ? 'update card' : 'create card'}
			</Button>
		</form>
	);
};

export const Form = withStyles(styles)(FormComponent);
