import React, { FC, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import {
	Card as CardMUI,
	CardContent,
	Grid,
	Typography,
	withStyles,
	createStyles,
	WithStyles,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import { PriorityIcon } from './priority-icon';

import { ICard } from '../types';
import { hideCard, toggleModal } from '../store/actions';

const styles = createStyles({
	card: {
		marginBottom: 12,
	},
	hover: {
		cursor: 'pointer',
	},
});

export type TCard = ICard & WithStyles<typeof styles>;

const CardComponent: FC<TCard> = ({
	id,
	name,
	description,
	priority,
	createDate,
	classes,
}) => {
	const dispatch = useDispatch();
	const hideCardAction = useCallback((cardId) => dispatch(hideCard(cardId)), [
		dispatch,
	]);
	const updateCardAction = useCallback(
		(cardId) => dispatch(toggleModal({ isOpen: true, cardId })),
		[dispatch]
	);

	const handleHideCard = () => hideCardAction(id);
	const handleUpdateCard = () => updateCardAction(id);

	return (
		<CardMUI className={classes.card}>
			<CardContent>
				<Grid container justify="space-between">
					<Grid item>
						<Typography variant="body1" component="p">
							<PriorityIcon priority={priority} />
							{name}
						</Typography>
					</Grid>
					<Grid item>
						<DeleteIcon
							className={classes.hover}
							onClick={handleHideCard}
							color="secondary"
						/>
						<EditIcon
							className={classes.hover}
							onClick={handleUpdateCard}
							color="action"
						/>
					</Grid>
				</Grid>
				<Typography variant="caption" component="p" color="textSecondary">
					{description}
				</Typography>
				<Typography variant="caption" component="p" color="textSecondary">
					{new Date(createDate).toLocaleDateString('ru', {
						hour: '2-digit',
						minute: '2-digit',
						timeZone: 'UTC',
					})}
				</Typography>
			</CardContent>
		</CardMUI>
	);
};

export const Card = withStyles(styles)(CardComponent);
