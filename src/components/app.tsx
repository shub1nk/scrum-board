import React, { FC, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Grid, withStyles, createStyles, WithStyles } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import cn from 'classnames';

import { Column } from './column';
import { Modal } from './modal';

import { IColumn } from '../types';
import { TRootState } from '../store';
import { toggleModal } from '../store/actions';

const styles = createStyles({
	heading: { textAlign: 'center' },
	addCard: { color: 'green', position: 'absolute', right: 20, top: 20 },
	hover: { cursor: 'pointer' },
	wrapper: { position: 'relative' },
});

export type TApp = WithStyles<typeof styles>;

const AppComponent: FC<TApp> = ({ classes }) => {
	const columns = useSelector<TRootState, IColumn[]>(
		({ board }) => board.columns
	);

	const dispatch = useDispatch();
	const addCard = useCallback(() => dispatch(toggleModal({ isOpen: true })), [
		dispatch,
	]);

	return (
		<section className={classes.wrapper}>
			<h1 className={classes.heading}>ScrumBoard</h1>
			<Grid container justify="center" spacing={3}>
				{columns.map((col: IColumn) => (
					<Column key={col.code} code={col.code} heading={col.heading} />
				))}
			</Grid>
			<AddCircleIcon
				className={cn(classes.addCard, classes.hover)}
				onClick={addCard}
				fontSize="large"
				titleAccess="add card"
			/>
			<Modal />
		</section>
	);
};

export const App = withStyles(styles)(AppComponent);
