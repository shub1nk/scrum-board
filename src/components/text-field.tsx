import React, { FC } from 'react';
import {
	TextField as TextFieldMUI,
	createStyles,
	withStyles,
	WithStyles,
} from '@material-ui/core';

import { ITextField } from '../types';

const styles = createStyles({
	textField: { marginBottom: 12, display: 'block' },
});

export type TTextField = ITextField & WithStyles<typeof styles>;

const TextFieldComponent: FC<TTextField> = ({
	id,
	label,
	value,
	onChangeHandler,
	classes,
}) => {
	return (
		<div className={classes.textField}>
			<TextFieldMUI
				id={id}
				label={label}
				value={value}
				onChange={onChangeHandler}
				variant="outlined"
			/>
		</div>
	);
};

export const TextField = withStyles(styles)(TextFieldComponent);
