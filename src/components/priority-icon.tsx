import React, { FC } from 'react';
import FiberManualRecordRoundedIcon from '@material-ui/icons/FiberManualRecordRounded';
import { withStyles, createStyles, WithStyles } from '@material-ui/core';

import { IPriorityIcon } from '../types';

const styles = createStyles({
	priority: { height: 12, width: 12, marginRight: 4 },
});

export type TPriorityIcon = IPriorityIcon & WithStyles<typeof styles>;

const priorityData = {
	low: { color: 'grey', weight: 1 },
	normal: { color: 'green', weight: 2 },
	high: { color: 'red', weight: 3 },
};

const PriorityIconComponent: FC<TPriorityIcon> = ({ priority, classes }) => (
	<FiberManualRecordRoundedIcon
		fontSize="small"
		htmlColor={priorityData[priority].color}
		className={classes.priority}
		titleAccess={priority}
	/>
);

export const PriorityIcon = withStyles(styles)(PriorityIconComponent);
