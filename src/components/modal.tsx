import React, { FC, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	Modal as ModakMUI,
	Paper,
	withStyles,
	createStyles,
	WithStyles,
} from '@material-ui/core';

import { TRootState } from '../store';
import { toggleModal } from '../store/actions';
import { Form } from './form';
import { ICard, IModalData } from '../types';

const styles = createStyles({
	modal: { padding: 24, width: '50vw', margin: '0 auto' },
});

export type TModal = WithStyles<typeof styles>;

const ModalComponent: FC<TModal> = ({ classes }) => {
	const { isOpen, cardId } = useSelector<TRootState, IModalData>(
		({ board }) => board.modal
	);
	const card = useSelector<TRootState, ICard | undefined>(({ board }) =>
		board.cards.find((card) => card.id === cardId)
	);

	const dispatch = useDispatch();
	const onClose = useCallback(() => dispatch(toggleModal({ isOpen: false })), [
		dispatch,
	]);

	return (
		<ModakMUI className={classes.modal} open={isOpen} onClose={onClose}>
			<Paper>
				<Form card={card} />
			</Paper>
		</ModakMUI>
	);
};

export const Modal = withStyles(styles)(ModalComponent);
