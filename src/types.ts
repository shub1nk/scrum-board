import { ChangeEvent } from 'react';

export interface ICard {
	/** Идентификатор карточки */
	id: number;
	/** Имя карточки */
	name: string;
	/** Описание карточки */
	description: string;
	/** Приоритет у карточки */
	priority: TPriority;
	/** Дата создания карточки */
	createDate: number;
	/** Статус карточки */
	status: TColumnName;
	/** Флаг. Видимость карточки */
	isHidden?: boolean;
}

export interface IColumn {
	/** Заголовок колонки */
	heading: string;
	/** Идентификатор колонки */
	code: TColumnName;
}

export interface IScrumBoard {
	/** Набор колонок */
	columns: IColumn[];
	/** Набор карточек */
	cards: ICard[];
}

export interface IModalData {
	/** Флаг. Открыта модалка или нет */
	isOpen: boolean;
	/** Идентификатор карточки. Если передан, значит подтянуть данные этой карточки для редактирования */
	cardId?: number;
}

export interface IForm {
	/** Данные карточки */
	card: ICard;
}

export interface IPriorityIcon {
	/** Приоритет */
	priority: TPriority;
}

export interface ITextField {
	/** Идентификатор */
	id: string;
	/** Лейбл */
	label: string;
	/** Значение поля */
	value: string;
	/** Хелдлер на изменение поля */
	// eslint-disable-next-line no-unused-vars
	onChangeHandler: (e: ChangeEvent<HTMLInputElement>) => void;
}

export interface ISelect {
	/** Опции */
	options: Array<TColumnName | TPriority>;
	/** Хелдлер на изменение поля */
	// eslint-disable-next-line no-unused-vars
	onChangeHandler: (e: ChangeEvent<{ value: TColumnName | TPriority }>) => void;
	/** Значение селекта */
	value: string;
}

export type TPriority = 'high' | 'normal' | 'low';
export type TColumnName = 'todo' | 'inProgress' | 'done';
