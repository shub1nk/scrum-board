const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/index.tsx',
	output: {
		path: path.join(__dirname, '/dist'),
		filename: 'bundle.min.js',
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.json'],
	},
	devServer: {
		contentBase: 'dist',
		compress: true,
		port: 3000,
		hot: true,
	},
	devtool: 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'ts-loader',
					},
				],
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './template/index.html',
			title: 'Scrum-board',
		}),
	],
};
